<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@home');
Route::get('/register',"AuthController@register");
Route::post('/welcome',"AuthController@sign_in");
Route::get('/data-tables',"IndexController@table");

//CRUD
//Create
Route::get('/cast/create','CastController@create');//menampilkan form untuk membuat data pemain film baru
Route::post('/cast','CastController@store');//	menyimpan data baru ke tabel Cast

//Read
Route::get('/cast', 'CastController@index');// tampilkan ke blade
Route::get('/cast/{cast_id}','CastController@show');//route detail cast

// Update
Route::get('/cast/{cast_id}/edit','CastController@edit');// Edit cast
Route::put('/cast/{cast_id}','CastController@update');// Untuk Update

// Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy');// untuk menghapus