@extends('layout.master')

@section('judul')
  Halaman tambabh cast 
@endsection
@section('content')

<form action="/cast" method="post">
    @csrf
    <div class="form-grup">
        <label>name :</label>
        <input type="text"name='nama' class="form-control">
        
    @error('nama')
        <div class="alert alert-denger">{{$message}}</div>
    @enderror
    <div class="form-grup">
        <label>umur :</label>
        <input type="number"name='umur' class="form-control">
        
    @error('umur')
        <div class="alert alert-denger">{{$message}}</div>
    @enderror
    <div class="form-grup">
        <label>bio:</label>
        <textarea name="bio" id="" cols="30" rows="10" class="form-control"></textarea>
        
    @error('bio')
        <div class="alert alert-denger">{{$message}}</div>
    @enderror 
    <br>
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="/cast" class="btn btn-dark">cancel</a>
</form>  
@endsection


